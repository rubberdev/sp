<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoer nieuwe data</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <style>
        body {
            padding-top: 50px;
            background-color: #5CB85C;
        }

        .container {
            background-color: #ffffff;
            padding-top: 15px;
        }

        a,
        a:hover,
        a:focus {
            color: #ed1c24;
        }

        button {
            cursor: pointer;
        }
    </style>

    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
</head>
<body>
<main>
    <div class="container">
        <div class="jumbotron">
            <h1>Data invoer voor verkiezing:</h1>
            <h2>Gemeenteraad Zwolle 2014</h2>
        </div>
        <p>Velden met een <sup>*</sup> zijn verplicht.</p>
        <form action="gr2014-submit.php" method="post" name="input-data">
            <h3>Stembureau</h3>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="stembureau__naam">Naam<sup>*</sup></label>
                        <input class="form-control" type="text" id="stembureau__naam" name="stembureau[naam]" required
                               pattern="^.{1,45}$">
                        <small class="form-text text-muted">Maximaal 45 karakters.</small>
                    </div>
            </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="stembureau__nummer">Nummer</label>
                        <input class="form-control" type="text" id="stembureau__nummer" name="stembureau[nummer]"
                               pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="stembureau__postcode">Postcode <sup>*</sup></label>
                        <input class="form-control" type="text" id="stembureau__postcode" name="stembureau[postcode]"
                               required pattern="^\d{4}\s?\w{2}$">
                        <small class="form-text text-muted">Voorbeeld: 1234 AB</small>
                    </div>
                </div>
            </div>
            <hr>
            <h3>Stembureau statistieken</h3>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="stembureau__stats__opgeroepen">Opgeroepen stemgerechtigden<sup>*</sup></label>
                        <input class="form-control" type="text" id="stembureau__stats__opgeroepen"
                               name="stembureau_stats[opgeroepen]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="stembureau__stats__opkomst">Opkomst<sup>*</sup></label>
                        <input class="form-control" type="text" id="stembureau__stats__opkomst"
                               name="stembureau_stats[opkomst]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
            </div>
            <hr>
            <h3>Stemmen per partij</h3>
            <p>Als een partij geen stemmen heeft dit invullen als '0'.</p>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__pvda">PvdA<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__pvda"
                               name="stemmen_stembureau_per_partij[pvda]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__vvd">VVD<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__vvd"
                               name="stemmen_stembureau_per_partij[vvd]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__cda">CDA<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__cda"
                               name="stemmen_stembureau_per_partij[cda]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__cu">CU<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__cu"
                               name="stemmen_stembureau_per_partij[cu]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__gl">GL<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__gl"
                               name="stemmen_stembureau_per_partij[gl]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__sw">SW<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__sw"
                               name="stemmen_stembureau_per_partij[sw]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__d66">D66<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__d66"
                               name="stemmen_stembureau_per_partij[d66]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__sp">SP<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__sp"
                               name="stemmen_stembureau_per_partij[sp]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__pp">PP<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__pp"
                               name="stemmen_stembureau_per_partij[pp]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__opa">OPA<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__opa"
                               name="stemmen_stembureau_per_partij[opa]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="stemmen__stembureau__per__partij__lwl">LWL<sup>*</sup></label>
                        <input class="form-control" type="text" id="stemmen__stembureau__per__partij__lwl"
                               name="stemmen_stembureau_per_partij[lwl]" required pattern="^\d{1,11}$">
                        <small class="form-text text-muted">Alleen getallen. Maximaal 11 karakters.</small>
                    </div>
                </div>
            </div>
            <hr>
            <button class="btn btn-block btn-success" type="submit">Opslaan</button>
            <br><br><br>
        </form>
    </div>
</main>
</body>
</html>