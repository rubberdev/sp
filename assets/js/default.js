var stembureauOpkomst = 0;
var stembureauPostcode = '';
var colorScheme = [
    '#2A1962',
    '#35236B',
    '#403075',
    '#4D3E7F',
    '#5B4D88',
    '#172D5F',
    '#223769',
    '#2E4172',
    '#3C4F7B',
    '#4B5C85',
    '#0E5056',
    '#18595F',
    '#236167',
    '#306B70',
    '#3E7378',
    '#116D21',
    '#1D772D',
    '#2B823A',
    '#3C8D4A',
    '#4E975B',
    '#8E8E16',
    '#9C9C26',
    '#AAAA39',
    '#B8B84E',
    '#C6C665',
    '#8E7316',
    '#9C8126',
    '#AA9039',
    '#B8A04E',
    '#C6B065'
];

var gMapsApiKey = 'AIzaSyCzDLr7MZjkWWcIKvM7yQjnMaJKy4nd0Fg';

jQuery(document).ready(function ($) {
    $(document).on('click', '.stembureau-row', function () {
        getStembureau($(this).data('stembureau_id'));
    });
});

function getStembureau(stembureauId) {
    $.ajax({
        url: "api.php?action=getStembureau&stembureauId=" + stembureauId,
        type: "GET",
        dataType: "html",
        success: function (result) {
            fillModal1(result);
            getStemmen(stembureauId);
        },
        error: function (xhr, status, errorThrown) {
            alert("Sorry, there was a problem!");
            console.log("Error: " + errorThrown);
            console.log("Status: " + status);
            console.dir(xhr);
        }
    });
}

function getStemmen(stembureauId) {
    $.ajax({
        url: "api.php?action=getStemmen&stembureauId=" + stembureauId,
        type: "GET",
        dataType: "html",
        success: function (result) {
            fillModal2(result);
            var modal = $('[data-remodal-id=modal]').remodal();
            modal.open();
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (xhr, status, errorThrown) {
            alert("Sorry, there was a problem!");
            console.log("Error: " + errorThrown);
            console.log("Status: " + status);
            console.dir(xhr);
        }
    });
}

function fillModal1(data) {
    console.log(data);
    data = JSON.parse(data);

    if (data.nummer === null) {
        data.nummer = 'Niet opgegeven';
    }

    if (data.locatie_lat === null) {
        data.locatie_lat = 'Niet opgegeven';
    }

    if (data.locatie_lon === null) {
        data.locatie_lon = 'Niet opgegeven';
    }

    stembureauOpkomst = data.opkomst;

    var htm = '<table class="table table-bordered table-striped">' +
        '<tr>' +
        '<td>Stembureau ID</td>' +
        '<td>' + data.stembureau_id + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Naam</td>' +
        '<td>' + data.naam + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Nummer</td>' +
        '<td>' + data.nummer + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Postcode</td>' +
        '<td>' + data.postcode + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Opgeroepen stemgerechtigden</td>' +
        '<td>' + data.opgeroepen + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Opkomst</td>' +
        '<td>' + data.opkomst + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Opkomst percentage</td>' +
        '<td>' + ((data.opkomst / data.opgeroepen * 100).toFixed(2)) + '%</td>' +
        '</tr>' +
        '</table>';

    stembureauPostcode = data.postcode;

    $('.m-stembureau-stats').html(htm);
}

function fillModal2(data) {
    data = JSON.parse(data);

    var piechartData = [];
    var piechartLabels = [];

    var postcodeArray = stembureauPostcode.split(' ');

    var htm = '<p>Grafiek toont geen partijen zonder stemmen.</p>' +
        '<div class="pie-chart-wrapper">' +
        '<canvas id="stembureauPieChart" width="400" height="400"></canvas>' +
        '<iframe class="google-maps" width="100%" height="400" src="//www.google.com/maps/embed/v1/place?q='+postcodeArray[0]+'+'+postcodeArray[1]+'+Nederland&zoom=17&key='+gMapsApiKey+'"></iframe>' +
        '</div>';

    htm += '<p>Tabel toont geen partijen zonder stemmen.</p>' +
        '<table class="table table-bordered table-striped">' +
        '<thead>' +
        '<tr>' +
        '<th>Partij</th>' +
        '<th>Stemmen</th>' +
        '<th>Percentage</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';

    for (var i = 0; i < data.length; i++) {
        if(data[i].stemmen === 0) {
            continue;
        }

        piechartData.push(data[i].stemmen);
        piechartLabels.push(data[i].afkorting);

        htm += '<tr>' +
            '<td data-toggle="tooltip" data-placement="left" title="' + data[i].naam + '">' + data[i].afkorting + '</td>' +
            '<td>' + data[i].stemmen + '</td>' +
            '<td>' + ((data[i].stemmen / stembureauOpkomst * 100).toFixed(2)) + '%</td>' +
            '</tr>';
    }

    htm += '</tbody>' +
        '</table>';

    $('.m-stemmen').html(htm);

    /*region PIE CHART*/
    var ctx = $('#stembureauPieChart');

    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: piechartData,
                backgroundColor: colorScheme,
                borderWidth: 1
            }],
            labels: piechartLabels
        }
    });
    /*endregion*/

}