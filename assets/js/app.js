var spApp = angular.module("spApp", ['naturalSort']);

spApp.controller('spController', function ($scope, $http) {
    $scope.sortType = 'stembureau_id';
    $scope.sortReverse = false;
    $scope.searchField = '';
    $scope.sortedField = '';

    var url = 'api.php?action=getOverview';

    $http({
        method: 'GET',
        url: url
    }).then(function (success) {
        $scope.tabledata = success.data;

        for (var i = 0; i < $scope.tabledata.length; i++) {
            var $opgeroepen = $scope.tabledata[i].opgeroepen;
            var $opkomst = $scope.tabledata[i].opkomst;
            var $stemmenSp = $scope.tabledata[i].stemmen_partij;
            $scope.tabledata[i].percentage_opkomst = (($opkomst / $opgeroepen) * 100).toFixed(2).replace('.', ',');
            $scope.tabledata[i].percentage_sp = (($stemmenSp / $opkomst) * 100).toFixed(2).replace('.', ',');
        }
    }, function (error) {
        console.log(error);
    })
});