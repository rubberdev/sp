<?php
include_once 'pdo.php';
$db = new database();

$sb_data = $_POST['stembureau'];
$sbs_data = $_POST['stembureau_stats'];
$sbspp_data = $_POST['stemmen_stembureau_per_partij'];

if (empty($sb_data['nummer'])) {
    $sb_data['nummer'] = null;
}

if (empty($sb_data['lon'])) {
    $sb_data['lon'] = null;
}

if (empty($sb_data['lat'])) {
    $sb_data['lat'] = null;
}

//region sb_data
$sb_data_query = "INSERT INTO stembureau (naam, nummer, postcode)
                    VALUES (:naam, :nummer, :postcode)";

$db->query($sb_data_query);
$db->bind(':naam', $sb_data['naam']);
$db->bind(':nummer', $sb_data['nummer']);
$db->bind(':postcode', $sb_data['postcode']);
$db->execute();
//endregion

//region stembureau_id
$sb_data_query_select = "SELECT stembureau_id FROM stembureau ORDER BY stembureau_id DESC LIMIT 1";

$db->query($sb_data_query_select);
$res = $db->single(false);
$stembureau_id = $res['stembureau_id'];
//endregion

//region sbs_data
$sbs_data_query = "INSERT INTO stembureau_stats (verkiezing_id, stembureau_id, opgeroepen, opkomst)
                    VALUES (1, :stembureau_id, :opgeroepen, :opkomst)";

$db->query($sbs_data_query);
$db->bind(':stembureau_id', $stembureau_id);
$db->bind(':opgeroepen', $sbs_data['opgeroepen']);
$db->bind(':opkomst', $sbs_data['opkomst']);
$db->execute();
//endregion

//region sbspp_data
$sbspp_data_query = "INSERT INTO stemmen_stembureau_per_partij (verkiezing_id, stembureau_id, partij_id, stemmen_partij)
                        VALUES (1, $stembureau_id, 1, :vvd),
                        (1, $stembureau_id, 2, :pvda),
                        (1, $stembureau_id, 3, :pvv),
                        (1, $stembureau_id, 4, :cda),
                        (1, $stembureau_id, 5, :sp),
                        (1, $stembureau_id, 6, :d66),
                        (1, $stembureau_id, 7, :gl),
                        (1, $stembureau_id, 8, :cu),
                        (1, $stembureau_id, 9, :sgp),
                        (1, $stembureau_id, 10, :pvdd),
                        (1, $stembureau_id, 11, :pp),
                        (1, $stembureau_id, 12, :mens),
                        (1, $stembureau_id, 13, :nl),
                        (1, $stembureau_id, 14, :lp),
                        (1, $stembureau_id, 15, :dpk),
                        (1, $stembureau_id, 16, :50plus),
                        (1, $stembureau_id, 17, :libdem),
                        (1, $stembureau_id, 18, :aep),
                        (1, $stembureau_id, 19, :sopn),
                        (1, $stembureau_id, 20, :pvdt)";

$db->query($sbspp_data_query);
$db->bind(':vvd', $sbspp_data['vvd']);
$db->bind(':pvda', $sbspp_data['pvda']);
$db->bind(':pvv', $sbspp_data['pvv']);
$db->bind(':cda', $sbspp_data['cda']);
$db->bind(':sp', $sbspp_data['sp']);
$db->bind(':d66', $sbspp_data['d66']);
$db->bind(':gl', $sbspp_data['gl']);
$db->bind(':cu', $sbspp_data['cu']);
$db->bind(':sgp', $sbspp_data['sgp']);
$db->bind(':pvdd', $sbspp_data['pvdd']);
$db->bind(':pp', $sbspp_data['pp']);
$db->bind(':mens', $sbspp_data['mens']);
$db->bind(':nl', $sbspp_data['nl']);
$db->bind(':lp', $sbspp_data['lp']);
$db->bind(':dpk', $sbspp_data['dpk']);
$db->bind(':50plus', $sbspp_data['50plus']);
$db->bind(':libdem', $sbspp_data['libdem']);
$db->bind(':aep', $sbspp_data['aep']);
$db->bind(':sopn', $sbspp_data['sopn']);
$db->bind(':pvdt', $sbspp_data['pvdt']);
$db->execute();
//endregion

echo '<h2>Data succesvol opgeslagen</h2>';
echo '<script>
    setTimeout(function () {
        window.location.href = "./index.php ";
    }, 2000);
</script>';