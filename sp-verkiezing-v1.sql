-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 29 nov 2017 om 21:58
-- Serverversie: 5.6.26
-- PHP-versie: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sp-verkiezing`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `partij`
--

CREATE TABLE IF NOT EXISTS `partij` (
  `partij_id` int(11) NOT NULL,
  `partijnaam` varchar(45) DEFAULT NULL,
  `partij_afkorting` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `partij`
--

INSERT INTO `partij` (`partij_id`, `partijnaam`, `partij_afkorting`) VALUES
(1, 'Volkspartij voor Vrijheid en Democratie', 'VVD'),
(2, 'Partij van de Arbeid', 'PvdA'),
(3, 'Partij voor de Vrijheid', 'PVV'),
(4, 'Christen Democratisch Appèl', 'CDA'),
(5, 'Socialistische Partij', 'SP'),
(6, 'Democraten 66', 'D66'),
(7, 'GroenLinks', 'GL'),
(8, 'ChristenUnie', 'CU'),
(9, 'Staatkundig Gereformeerde Partij', 'SGP'),
(10, 'Partij voor de Dieren', 'PvdD'),
(11, 'Piratenpartij', 'PP'),
(12, 'Partij voor Mens en Spirit', 'MenS'),
(13, 'Nederland Lokaal', 'NL'),
(14, 'Libertarische Partij', 'LP'),
(15, 'Democratisch Politiek Keerpunt', 'DPK'),
(16, '50PLUS', '50PLUS'),
(17, 'Liberaal Democratische Partij', 'LibDem'),
(18, 'Anti Europa Partij', 'AEP'),
(19, 'Soeverein Onafhankelijke Pioniers Nederland', 'SOPN'),
(20, 'Partij van de Toekomst', 'PvdT'),
(21, 'Swollwacht', 'SW'),
(22, 'Ouderen Politiek Actief', 'OPA'),
(23, 'Lijst Willem Lucht', 'LWL'),
(24, 'OndernemersPartij', 'OP'),
(25, 'VoorNederland', 'VNL'),
(26, 'DENK', 'DENK'),
(27, 'NIEUWE WEGEN', 'NW'),
(28, 'Forum voor Democratie', 'FvD'),
(29, 'De Burger Beweging', 'DBB'),
(30, 'Vrijzinnige Partij', 'VP'),
(31, 'GeenPeil', 'GP'),
(32, 'Artikel 1', 'Art1kel'),
(33, 'Niet Stemmers', 'NS'),
(34, 'Lokaal in de Kamer', 'LidK');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `stembureau`
--

CREATE TABLE IF NOT EXISTS `stembureau` (
  `stembureau_id` int(11) NOT NULL,
  `naam` varchar(45) DEFAULT NULL,
  `nummer` int(11) DEFAULT NULL,
  `postcode` varchar(7) DEFAULT NULL,
  `locatie_lat` varchar(45) DEFAULT NULL,
  `locatie_lon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `stembureau`
--

INSERT INTO `stembureau` (`stembureau_id`, `naam`, `nummer`, `postcode`, `locatie_lat`, `locatie_lon`) VALUES
(1, 'Stadhuis', NULL, '8011 PK', '6.092781', '52.511354'),
(2, 'Het Lumeijn', NULL, '8019 AL', '6.076583', '52.510711'),
(3, 'Woonzorgcentrum De Nieuwe Haven', NULL, '8011 AS', '6.083325', '52.511353'),
(4, 'GGD IJsselland', NULL, '8011 CV', '6.093504', '52.508203'),
(5, 'Wijkcentrum De Enk', NULL, '8012 VA', '6.099045', '52.506798'),
(6, 'Het Mozaïek locatie De Mozadance', NULL, '8012 DJ', '6.103490', '52.504717'),
(7, 'Woonzorgcentrum De Molenhof', NULL, '8012 EJ', NULL, NULL),
(8, 'Stadhuis', NULL, '8011 PK', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `stembureau_stats`
--

CREATE TABLE IF NOT EXISTS `stembureau_stats` (
  `verkiezing_id` int(11) NOT NULL,
  `stembureau_id` int(11) NOT NULL,
  `opgeroepen` int(11) DEFAULT NULL,
  `opkomst` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `stembureau_stats`
--

INSERT INTO `stembureau_stats` (`verkiezing_id`, `stembureau_id`, `opgeroepen`, `opkomst`) VALUES
(1, 1, 1686, 1515),
(1, 2, 1734, 1294),
(1, 3, 1137, 629),
(1, 4, 1573, 1099),
(1, 5, 1465, 1485),
(1, 6, 1479, 1024),
(1, 7, 1326, 954),
(3, 8, 1622, 1858);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `stemmen_stembureau_per_partij`
--

CREATE TABLE IF NOT EXISTS `stemmen_stembureau_per_partij` (
  `verkiezing_id` int(11) NOT NULL,
  `stembureau_id` int(11) NOT NULL,
  `partij_id` int(11) NOT NULL,
  `stemmen_partij` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `stemmen_stembureau_per_partij`
--

INSERT INTO `stemmen_stembureau_per_partij` (`verkiezing_id`, `stembureau_id`, `partij_id`, `stemmen_partij`) VALUES
(1, 1, 1, 417),
(1, 1, 2, 386),
(1, 1, 3, 46),
(1, 1, 4, 72),
(1, 1, 5, 114),
(1, 1, 6, 282),
(1, 1, 7, 58),
(1, 1, 8, 68),
(1, 1, 9, 9),
(1, 1, 10, 36),
(1, 1, 11, 6),
(1, 1, 12, 3),
(1, 1, 13, 0),
(1, 1, 14, 0),
(1, 1, 15, 0),
(1, 1, 16, 10),
(1, 1, 17, 1),
(1, 1, 18, 0),
(1, 1, 19, 4),
(1, 1, 20, 3),
(1, 2, 1, 403),
(1, 2, 2, 268),
(1, 2, 3, 45),
(1, 2, 4, 97),
(1, 2, 5, 88),
(1, 2, 6, 167),
(1, 2, 7, 56),
(1, 2, 8, 117),
(1, 2, 9, 7),
(1, 2, 10, 22),
(1, 2, 11, 5),
(1, 2, 12, 1),
(1, 2, 13, 0),
(1, 2, 14, 0),
(1, 2, 15, 0),
(1, 2, 16, 13),
(1, 2, 17, 0),
(1, 2, 18, 1),
(1, 2, 19, 2),
(1, 2, 20, 2),
(1, 3, 1, 74),
(1, 3, 2, 185),
(1, 3, 3, 77),
(1, 3, 4, 69),
(1, 3, 5, 64),
(1, 3, 6, 49),
(1, 3, 7, 18),
(1, 3, 8, 36),
(1, 3, 9, 5),
(1, 3, 10, 25),
(1, 3, 11, 10),
(1, 3, 12, 1),
(1, 3, 13, 1),
(1, 3, 14, 0),
(1, 3, 15, 2),
(1, 3, 16, 12),
(1, 3, 17, 0),
(1, 3, 18, 1),
(1, 3, 19, 0),
(1, 3, 20, 0),
(1, 4, 1, 211),
(1, 4, 2, 313),
(1, 4, 3, 26),
(1, 4, 4, 49),
(1, 4, 5, 93),
(1, 4, 6, 186),
(1, 4, 7, 65),
(1, 4, 8, 102),
(1, 4, 9, 6),
(1, 4, 10, 29),
(1, 4, 11, 11),
(1, 4, 12, 1),
(1, 4, 13, 0),
(1, 4, 14, 0),
(1, 4, 15, 0),
(1, 4, 16, 4),
(1, 4, 17, 0),
(1, 4, 18, 0),
(1, 4, 19, 3),
(1, 4, 20, 0),
(1, 5, 1, 179),
(1, 5, 2, 493),
(1, 5, 3, 46),
(1, 5, 4, 40),
(1, 5, 5, 197),
(1, 5, 6, 204),
(1, 5, 7, 96),
(1, 5, 8, 142),
(1, 5, 9, 9),
(1, 5, 10, 50),
(1, 5, 11, 7),
(1, 5, 12, 6),
(1, 5, 13, 1),
(1, 5, 14, 1),
(1, 5, 15, 0),
(1, 5, 16, 10),
(1, 5, 17, 0),
(1, 5, 18, 0),
(1, 5, 19, 4),
(1, 5, 20, 0),
(1, 6, 1, 135),
(1, 6, 2, 326),
(1, 6, 3, 53),
(1, 6, 4, 53),
(1, 6, 5, 119),
(1, 6, 6, 125),
(1, 6, 7, 69),
(1, 6, 8, 91),
(1, 6, 9, 3),
(1, 6, 10, 32),
(1, 6, 11, 5),
(1, 6, 12, 3),
(1, 6, 13, 0),
(1, 6, 14, 0),
(1, 6, 15, 0),
(1, 6, 16, 7),
(1, 6, 17, 0),
(1, 6, 18, 0),
(1, 6, 19, 3),
(1, 6, 20, 0),
(1, 7, 1, 125),
(1, 7, 2, 328),
(1, 7, 3, 52),
(1, 7, 4, 80),
(1, 7, 5, 99),
(1, 7, 6, 91),
(1, 7, 7, 43),
(1, 7, 8, 82),
(1, 7, 9, 8),
(1, 7, 10, 28),
(1, 7, 11, 6),
(1, 7, 12, 4),
(1, 7, 13, 0),
(1, 7, 14, 1),
(1, 7, 15, 1),
(1, 7, 16, 4),
(1, 7, 17, 0),
(1, 7, 18, 0),
(1, 7, 19, 2),
(1, 7, 20, 0),
(3, 8, 1, 448),
(3, 8, 2, 120),
(3, 8, 3, 57),
(3, 8, 4, 133),
(3, 8, 5, 101),
(3, 8, 6, 388),
(3, 8, 7, 314),
(3, 8, 8, 111),
(3, 8, 9, 8),
(3, 8, 10, 102),
(3, 8, 11, 9),
(3, 8, 16, 10),
(3, 8, 24, 0),
(3, 8, 25, 4),
(3, 8, 26, 6),
(3, 8, 27, 5),
(3, 8, 28, 36),
(3, 8, 29, 0),
(3, 8, 30, 1),
(3, 8, 31, 1),
(3, 8, 32, 1),
(3, 8, 33, 3),
(3, 8, 34, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `stemmen_totaal_partij`
--

CREATE TABLE IF NOT EXISTS `stemmen_totaal_partij` (
  `verkiezing_id` int(11) NOT NULL,
  `partij_id` int(11) NOT NULL,
  `stemmen_partij` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `stemmen_totaal_partij`
--

INSERT INTO `stemmen_totaal_partij` (`verkiezing_id`, `partij_id`, `stemmen_partij`) VALUES
(1, 1, 14198),
(1, 2, 21411),
(1, 3, 4328),
(1, 4, 5403),
(1, 5, 6878),
(1, 6, 6696),
(1, 7, 2275),
(1, 8, 7355),
(1, 9, 769),
(1, 10, 1288),
(1, 11, 289),
(1, 12, 154),
(1, 13, 13),
(1, 14, 19),
(1, 15, 28),
(1, 16, 871),
(1, 17, 8),
(1, 18, 14),
(1, 19, 121),
(1, 20, 43),
(2, 1, 6646),
(2, 2, 8141),
(2, 4, 4812),
(2, 5, 6612),
(2, 6, 7781),
(2, 7, 4680),
(2, 8, 10478),
(2, 11, 798),
(2, 21, 3994),
(2, 22, 1149),
(2, 23, 237),
(3, 1, 14074),
(3, 2, 5700),
(3, 3, 6379),
(3, 4, 9060),
(3, 5, 6967),
(3, 6, 11912),
(3, 7, 9374),
(3, 8, 8334),
(3, 9, 798),
(3, 10, 2727),
(3, 11, 330),
(3, 16, 1897),
(3, 24, 78),
(3, 25, 198),
(3, 26, 966),
(3, 27, 85),
(3, 28, 1121),
(3, 29, 44),
(3, 30, 35),
(3, 31, 37),
(3, 32, 69),
(3, 33, 62),
(3, 34, 43);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `verkiezing`
--

CREATE TABLE IF NOT EXISTS `verkiezing` (
  `verkiezing_id` int(11) NOT NULL,
  `datum` varchar(8) NOT NULL,
  `naam` varchar(60) DEFAULT NULL,
  `cbs_nummer` varchar(45) DEFAULT NULL,
  `totaal_opgeroepen` int(11) DEFAULT NULL,
  `totaal_stemmen` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `verkiezing`
--

INSERT INTO `verkiezing` (`verkiezing_id`, `datum`, `naam`, `cbs_nummer`, `totaal_opgeroepen`, `totaal_stemmen`) VALUES
(1, '20120912', 'Tweede Kamer der Staten-Generaal 2012', '193', 93106, 72161),
(2, '20140919', 'Gemeenteraad Zwolle 2014', '193', 0, 55328),
(3, '20170315', 'Tweede Kamer der Staten-Generaal 2017', '193', 95706, 80290);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `partij`
--
ALTER TABLE `partij`
  ADD PRIMARY KEY (`partij_id`);

--
-- Indexen voor tabel `stembureau`
--
ALTER TABLE `stembureau`
  ADD PRIMARY KEY (`stembureau_id`);

--
-- Indexen voor tabel `stembureau_stats`
--
ALTER TABLE `stembureau_stats`
  ADD PRIMARY KEY (`verkiezing_id`,`stembureau_id`);

--
-- Indexen voor tabel `stemmen_stembureau_per_partij`
--
ALTER TABLE `stemmen_stembureau_per_partij`
  ADD PRIMARY KEY (`verkiezing_id`,`stembureau_id`,`partij_id`);

--
-- Indexen voor tabel `stemmen_totaal_partij`
--
ALTER TABLE `stemmen_totaal_partij`
  ADD PRIMARY KEY (`verkiezing_id`,`partij_id`);

--
-- Indexen voor tabel `verkiezing`
--
ALTER TABLE `verkiezing`
  ADD PRIMARY KEY (`verkiezing_id`),
  ADD UNIQUE KEY `datum_UNIQUE` (`datum`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `partij`
--
ALTER TABLE `partij`
  MODIFY `partij_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT voor een tabel `stembureau`
--
ALTER TABLE `stembureau`
  MODIFY `stembureau_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT voor een tabel `verkiezing`
--
ALTER TABLE `verkiezing`
  MODIFY `verkiezing_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
