<?php
session_start();
$_SESSION['vId'] = 2;

include_once 'pdo.php';
$db = new database();

$totals = 'SELECT * FROM verkiezing WHERE verkiezing_id = :verkiezingId';
$db->query($totals);
$db->bind(':verkiezingId', $_SESSION['vId']);
$res = $db->single(false);
?>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SP</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="assets/lib/remodal/remodal.css">
    <link rel="stylesheet" href="assets/lib/remodal/remodal-default-theme.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <script src="assets/lib/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
    <script src="assets/js/naturalSortVersionDates.min.js"></script>
</head>
<body>
<main ng-app="spApp" ng-controller="spController">
    <div class="container">
        <div class="alert alert-warning">
            <strong>Let Op!</strong><br>
            Omdat de totalen van sommige stembureaus in de data was samengevoegd, waardoor ik het gemiddelde heb
            genomen voor die stembureaus, hebben sommige stembureaus totalen (opgeroepen en opkomst) die niet persé
            overeenkomen met het daadwerkelijk aantal uitgebrachte stemmen. Het aantal uitgebrachte stemmen per partij
            is wel correct.<br><br>
            Het gaat om de volgende stembureau combinaties:<br><br>
            <table border="1">
                <tr>
                    <td style="padding: 0.5em 1em;">Combinatie 1</td>
                    <td style="padding: 0.5em 1em;">Stembureau 18 & 19</td>
                </tr>
                <tr>
                    <td style="padding: 0.5em 1em;">Combinatie 2</td>
                    <td style="padding: 0.5em 1em;">Stembureau 21 & 22</td>
                </tr>
                <tr>
                    <td style="padding: 0.5em 1em;">Combinatie 3</td>
                    <td style="padding: 0.5em 1em;">Stembureau 49 & 53</td>
                </tr>
                <tr>
                    <td style="padding: 0.5em 1em;">Combinatie 4</td>
                    <td style="padding: 0.5em 1em;">Stembureau 42, 50 & 62</td>
                </tr>
                <tr>
                    <td style="padding: 0.5em 1em;">Combinatie 5</td>
                    <td style="padding: 0.5em 1em;">Stembureau 39 & 40</td>
                </tr>
                <tr>
                    <td style="padding: 0.5em 1em;">Combinatie 6</td>
                    <td style="padding: 0.5em 1em;">Stembureau 44 & 52</td>
                </tr>
                <tr>
                    <td style="padding: 0.5em 1em;">Combinatie 7</td>
                    <td style="padding: 0.5em 1em;">Stembureau 55, 67 & 68</td>
                </tr>
            </table>
        </div>

        <table class="table table-bordered table-striped">
            <tr>
                <td>Verkiezing ID</td>
                <td><?= $res['verkiezing_id'] ?></td>
            </tr>
            <tr>
                <td>Datum</td>
                <td><?= date('d-m-Y', strtotime($res['datum'])) ?></td>
            </tr>
            <tr>
                <td>Naam</td>
                <td><?= $res['naam'] ?></td>
            </tr>
            <tr>
                <td>CBS nummer</td>
                <td><?= $res['cbs_nummer'] ?></td>
            </tr>
            <tr>
                <td>Totaal opgeroepen</td>
                <td><?= $res['totaal_opgeroepen'] ?></td>
            </tr>
            <tr>
                <td>Totaal uitgebrachte stemmen</td>
                <td><?= $res['totaal_stemmen'] ?></td>
            </tr>
            <tr>
                <td>Opkomstpercentage</td>
                <td><?= str_replace('.', ',', number_format(($res['totaal_stemmen'] / $res['totaal_opgeroepen'] * 100), 2)) ?>%</td>
            </tr>
        </table>

        <form>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                    <input type="text" class="form-control" placeholder="Zoeken" ng-model="searchData">
                </div>
            </div>
        </form>

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>
                    <a href="#"
                       ng-click="sortType = 'nummer'; sortReverse = !sortReverse; sortedField = 'nummer'">
                        Nummer
                        <span ng-show="sortType == 'nummer' && !sortReverse" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'nummer' && sortReverse" class="fa fa-caret-up"></span>
                    </a>
                </th>
                <th>
                    <a href="#" ng-click="sortType = 'naam'; sortReverse = !sortReverse; sortedField = 'naam'">
                        Naam stembureau
                        <span ng-show="sortType == 'naam' && !sortReverse" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'naam' && sortReverse" class="fa fa-caret-up"></span>
                    </a>
                </th>
                <th>
                    <a href="#"
                       ng-click="sortType = 'opgeroepen'; sortReverse = !sortReverse; sortedField = 'opgeroepen'">
                        Opgeroepen
                        <span ng-show="sortType == 'opgeroepen' && !sortReverse" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'opgeroepen' && sortReverse" class="fa fa-caret-up"></span>
                    </a>
                </th>
                <th>
                    <a href="#" ng-click="sortType = 'opkomst'; sortReverse = !sortReverse; sortedField = 'opkomst'">
                        Opkomst
                        <span ng-show="sortType == 'opkomst' && !sortReverse" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'opkomst' && sortReverse" class="fa fa-caret-up"></span>
                    </a>
                </th>
                <th>
                    <a href="#"
                       ng-click='sortType = natural("percentage_opkomst"); sortReverse = !sortReverse; sortedField = "percentage_opkomst"'>
                        Opkomst percentage
                        <span ng-show='!sortReverse && sortedField == "percentage_opkomst"'
                              class="fa fa-caret-down"></span>
                        <span ng-show='sortReverse && sortedField == "percentage_opkomst"'
                              class="fa fa-caret-up"></span>
                    </a>
                </th>
                <th>
                    <a href="#"
                       ng-click="sortType = 'stemmen_partij'; sortReverse = !sortReverse; sortedField = 'stemmen_partij'">
                        Stemmen SP
                        <span ng-show="sortType == 'stemmen_partij' && !sortReverse" class="fa fa-caret-down"></span>
                        <span ng-show="sortType == 'stemmen_partij' && sortReverse" class="fa fa-caret-up"></span>
                    </a>
                </th>
                <th>
                    <a href="#"
                       ng-click='sortType = natural("percentage_sp"); sortReverse = !sortReverse; sortedField = "percentage_sp"'>
                        Percentage SP
                        <span ng-show='!sortReverse && sortedField == "percentage_sp"'
                              class="fa fa-caret-down"></span>
                        <span ng-show='sortReverse && sortedField == "percentage_sp"'
                              class="fa fa-caret-up"></span>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr class="stembureau-row" data-stembureau_id="{{ td.stembureau_id }}"
                ng-repeat="td in tabledata | orderBy:sortType:sortReverse | filter:searchData">
                <td>{{ td.nummer }}</td>
                <td>{{ td.naam }}</td>
                <td>{{ td.opgeroepen }}</td>
                <td>{{ td.opkomst }}</td>
                <td>{{ td.percentage_opkomst }}%</td>
                <td>{{ td.stemmen_partij }}</td>
                <td>{{ td.percentage_sp }}%</td>
            </tr>
            </tbody>
        </table>
    </div>
</main>

<div data-remodal-id="modal">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="m-stembureau-stats"></div>
    <div class="m-stemmen"></div>
</div>

<script src="assets/lib/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<script src="assets/js/app.js"></script>

<script src="assets/lib/Chart.min.js"></script>


<script src="assets/lib/remodal/remodal.min.js"></script>
<script src="assets/js/default.js"></script>
</body>
</html>