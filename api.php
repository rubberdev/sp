<?php
session_start();

include_once 'pdo.php';
$db = new database();

$vId = $_SESSION['vId'];

$queryOverview = 'SELECT st.stembureau_id, st.nummer, st.naam, sts.opgeroepen, sts.opkomst, stspp.stemmen_partij
                        FROM stembureau st 
	                    JOIN stembureau_stats sts ON st.stembureau_id = sts.stembureau_id
	                    JOIN stemmen_stembureau_per_partij stspp ON sts.stembureau_id = stspp.stembureau_id
                    WHERE stspp.partij_id = 5
                    AND stspp.verkiezing_id = '.$vId.'
                    ORDER BY st.stembureau_id';

$queryDetails_stembureau = 'SELECT sb.*, opgeroepen, opkomst
                            FROM stembureau sb	
                                JOIN stembureau_stats sbs ON sb.stembureau_id = sbs.stembureau_id
                            WHERE verkiezing_id = '.$vId.'
                            AND sb.stembureau_id = :stembureauId';

$queryDetails_stemmen = 'SELECT p.partijnaam naam, p.partij_afkorting afkorting,sbspp.stemmen_partij stemmen
                            FROM stemmen_stembureau_per_partij sbspp
                                JOIN partij p ON sbspp.partij_id = p.partij_id
                            WHERE sbspp.verkiezing_id = '.$vId.'
                            AND sbspp.stembureau_id = :stembureauId';

if(!isset($_GET['action'])) {
    echo 'No action specified';
} else {
    if ($_GET['action'] == 'getOverview') {
        $db->query($queryOverview);
        $row = $db->resultset();

        echo $row;
    }

    if($_GET['action'] == 'getStembureau') {
        $db->query($queryDetails_stembureau);
        $db->bind(':stembureauId', $_GET['stembureauId']);
        $res = $db->single();

        echo $res;
    }

    if($_GET['action'] == 'getStemmen') {
        $db->query($queryDetails_stemmen);
        $db->bind(':stembureauId', $_GET['stembureauId']);
        $res = $db->resultset();

        echo $res;
    }
}

