<?php
include_once 'pdo.php';
$db = new database();

$sb_data = $_POST['stembureau'];
$sbs_data = $_POST['stembureau_stats'];
$sbspp_data = $_POST['stemmen_stembureau_per_partij'];

if (empty($sb_data['nummer'])) {
    $sb_data['nummer'] = null;
}

if (empty($sb_data['lon'])) {
    $sb_data['lon'] = null;
}

if (empty($sb_data['lat'])) {
    $sb_data['lat'] = null;
}

//region sb_data
$sb_data_query = "INSERT INTO stembureau (naam, nummer, postcode)
                    VALUES (:naam, :nummer, :postcode)";

$db->query($sb_data_query);
$db->bind(':naam', $sb_data['naam']);
$db->bind(':nummer', $sb_data['nummer']);
$db->bind(':postcode', $sb_data['postcode']);
$db->execute();
//endregion

//region stembureau_id
$sb_data_query_select = "SELECT stembureau_id FROM stembureau ORDER BY stembureau_id DESC LIMIT 1";

$db->query($sb_data_query_select);
$res = $db->single(false);
$stembureau_id = $res['stembureau_id'];
//endregion

//region sbs_data
$sbs_data_query = "INSERT INTO stembureau_stats (verkiezing_id, stembureau_id, opgeroepen, opkomst)
                    VALUES (3, :stembureau_id, :opgeroepen, :opkomst)";

$db->query($sbs_data_query);
$db->bind(':stembureau_id', $stembureau_id);
$db->bind(':opgeroepen', $sbs_data['opgeroepen']);
$db->bind(':opkomst', $sbs_data['opkomst']);
$db->execute();
//endregion

//region sbspp_data
$sbspp_data_query = "INSERT INTO stemmen_stembureau_per_partij (verkiezing_id, stembureau_id, partij_id, stemmen_partij)
                        VALUES (3, $stembureau_id, 1, :vvd),
                        (3, $stembureau_id, 2, :pvda),
                        (3, $stembureau_id, 3, :pvv),
                        (3, $stembureau_id, 5, :sp),
                        (3, $stembureau_id, 4, :cda),
                        (3, $stembureau_id, 6, :d66),
                        (3, $stembureau_id, 8, :cu),
                        (3, $stembureau_id, 7, :gl),
                        (3, $stembureau_id, 9, :sgp),
                        (3, $stembureau_id, 10, :pvdd),
                        (3, $stembureau_id, 16, :50plus),
                        (3, $stembureau_id, 24, :op),
                        (3, $stembureau_id, 25, :vnl),
                        (3, $stembureau_id, 26, :denk),
                        (3, $stembureau_id, 27, :nw),
                        (3, $stembureau_id, 28, :fvd),
                        (3, $stembureau_id, 29, :dbb),
                        (3, $stembureau_id, 30, :vp),
                        (3, $stembureau_id, 31, :gp),
                        (3, $stembureau_id, 11, :pp),
                        (3, $stembureau_id, 32, :artikel1),
                        (3, $stembureau_id, 33, :ns),
                        (3, $stembureau_id, 34, :lidk)";

$db->query($sbspp_data_query);
$db->bind(':vvd', $sbspp_data['vvd']);
$db->bind(':pvda', $sbspp_data['pvda']);
$db->bind(':pvv', $sbspp_data['pvv']);
$db->bind(':sp', $sbspp_data['sp']);
$db->bind(':cda', $sbspp_data['cda']);
$db->bind(':d66', $sbspp_data['d66']);
$db->bind(':cu', $sbspp_data['cu']);
$db->bind(':gl', $sbspp_data['gl']);
$db->bind(':sgp', $sbspp_data['sgp']);
$db->bind(':pvdd', $sbspp_data['pvdd']);
$db->bind(':50plus', $sbspp_data['50plus']);
$db->bind(':op', $sbspp_data['op']);
$db->bind(':vnl', $sbspp_data['vnl']);
$db->bind(':denk', $sbspp_data['denk']);
$db->bind(':nw', $sbspp_data['nw']);
$db->bind(':fvd', $sbspp_data['fvd']);
$db->bind(':dbb', $sbspp_data['dbb']);
$db->bind(':vp', $sbspp_data['vp']);
$db->bind(':gp', $sbspp_data['gp']);
$db->bind(':pp', $sbspp_data['pp']);
$db->bind(':artikel1', $sbspp_data['artikel1']);
$db->bind(':ns', $sbspp_data['ns']);
$db->bind(':lidk', $sbspp_data['lidk']);
$db->execute();
//endregion

echo '<h2>Data succesvol opgeslagen</h2>';
echo '<script>
    setTimeout(function () {
        window.location.href = "./index.php ";
    }, 2000);
</script>';