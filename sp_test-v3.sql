-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 25 nov 2017 om 00:38
-- Serverversie: 5.6.26
-- PHP-versie: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sp_test`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `partij`
--

CREATE TABLE IF NOT EXISTS `partij` (
  `partij_id` int(11) NOT NULL,
  `partijnaam` varchar(45) DEFAULT NULL,
  `partij_afkorting` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `partij`
--

INSERT INTO `partij` (`partij_id`, `partijnaam`, `partij_afkorting`) VALUES
(1, 'Volkspartij voor Vrijheid en Democratie', 'VVD'),
(2, 'Partij van de Arbeid', 'PvdA'),
(3, 'Partij voor de Vrijheid', 'PVV'),
(4, 'Christen Democratisch Appèl', 'CDA'),
(5, 'Socialistische Partij', 'SP'),
(6, 'Democraten 66', 'D66'),
(7, 'GroenLinks', 'GL'),
(8, 'ChristenUnie', 'CU'),
(9, 'Staatkundig Gereformeerde Partij', 'SGP'),
(10, 'Partij voor de Dieren', 'PvdD'),
(11, 'Piratenpartij', 'PP'),
(12, 'Partij voor Mens en Spirit', 'MenS'),
(13, 'Nederland Lokaal', 'NL'),
(14, 'Libertarische Partij', 'LP'),
(15, 'Democratisch Politiek Keerpunt', 'DPK'),
(16, '50PLUS', '50PLUS'),
(17, 'Liberaal Democratische Partij', 'LibDem'),
(18, 'Anti Europa Partij', 'AEP'),
(19, 'Soeverein Onafhankelijke Pioniers Nederland', 'SOPN'),
(20, 'Partij van de Toekomst', 'PvdT'),
(21, 'Swollwacht', 'SW'),
(22, 'Ouderen Politiek Actief', 'OPA'),
(23, 'Lijst Willem Lucht', 'LWL'),
(24, 'OndernemersPartij', 'OP'),
(25, 'VoorNederland', 'VNL'),
(26, 'DENK', 'DENK'),
(27, 'NIEUWE WEGEN', 'NW'),
(28, 'Forum voor Democratie', 'FvD'),
(29, 'De Burger Beweging', 'DBB'),
(30, 'Vrijzinnige Partij', 'VP'),
(31, 'GeenPeil', 'GP'),
(32, 'Artikel 1', 'Art1kel'),
(33, 'Niet Stemmers', 'NS'),
(34, 'Lokaal in de Kamer', 'LidK');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `partij`
--
ALTER TABLE `partij`
  ADD PRIMARY KEY (`partij_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `partij`
--
ALTER TABLE `partij`
  MODIFY `partij_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
