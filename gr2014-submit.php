<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'pdo.php';
$db = new database();

$sb_data = $_POST['stembureau'];
$sbs_data = $_POST['stembureau_stats'];
$sbspp_data = $_POST['stemmen_stembureau_per_partij'];

if (empty($sb_data['nummer'])) {
    $sb_data['nummer'] = null;
}

if (empty($sb_data['lon'])) {
    $sb_data['lon'] = null;
}

if (empty($sb_data['lat'])) {
    $sb_data['lat'] = null;
}

//region sb_data
$sb_data_query = "INSERT INTO stembureau (naam, nummer, postcode)
                    VALUES (:naam, :nummer, :postcode)";

$db->query($sb_data_query);
$db->bind(':naam', $sb_data['naam']);
$db->bind(':nummer', $sb_data['nummer']);
$db->bind(':postcode', $sb_data['postcode']);
$db->execute();
//endregion

//region stembureau_id
$sb_data_query_select = "SELECT stembureau_id FROM stembureau ORDER BY stembureau_id DESC LIMIT 1";

$db->query($sb_data_query_select);
$res = $db->single(false);
$stembureau_id = $res['stembureau_id'];
//endregion

//region sbs_data
$sbs_data_query = "INSERT INTO stembureau_stats (verkiezing_id, stembureau_id, opgeroepen, opkomst)
                    VALUES (2, :stembureau_id, :opgeroepen, :opkomst)";

$db->query($sbs_data_query);
$db->bind(':stembureau_id', $stembureau_id);
$db->bind(':opgeroepen', $sbs_data['opgeroepen']);
$db->bind(':opkomst', $sbs_data['opkomst']);
$db->execute();
//endregion

//region sbspp_data
$sbspp_data_query = "INSERT INTO stemmen_stembureau_per_partij (verkiezing_id, stembureau_id, partij_id, stemmen_partij)
                        VALUES (2, $stembureau_id, 2, :pvda),
                        (2, $stembureau_id, 1, :vvd),
                        (2, $stembureau_id, 4, :cda),
                        (2, $stembureau_id, 8, :cu),
                        (2, $stembureau_id, 7, :gl),
                        (2, $stembureau_id, 21, :sw),
                        (2, $stembureau_id, 6, :d66),
                        (2, $stembureau_id, 5, :sp),
                        (2, $stembureau_id, 11, :pp),
                        (2, $stembureau_id, 22, :opa),
                        (2, $stembureau_id, 23, :lwl)";

$db->query($sbspp_data_query);

$db->bind(':pvda', $sbspp_data['pvda']);
$db->bind(':vvd', $sbspp_data['vvd']);
$db->bind(':cda', $sbspp_data['cda']);
$db->bind(':cu', $sbspp_data['cu']);
$db->bind(':gl', $sbspp_data['gl']);
$db->bind(':sw', $sbspp_data['sw']);
$db->bind(':d66', $sbspp_data['d66']);
$db->bind(':sp', $sbspp_data['sp']);
$db->bind(':pp', $sbspp_data['pp']);
$db->bind(':opa', $sbspp_data['opa']);
$db->bind(':lwl', $sbspp_data['lwl']);
$db->execute();

//endregion

echo '<h2>Data succesvol opgeslagen</h2>';
echo '<script>
    setTimeout(function () {
        window.location.href = "./index.php ";
    }, 2000);
</script>';