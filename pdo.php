<?php

class database
{
    private $host = '127.0.0.1';
    private $db = 'sp_verkiezing';
    private $user = 'root';
    private $pass = '';
    private $charset = 'utf8';

    private $pdo;
    private $error;

    private $stmt;

    public function __construct()
    {
        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";

        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            $this->pdo = new PDO($dsn, $this->user, $this->pass, $opt);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    public function query($query)
    {
        $this->stmt = $this->pdo->prepare($query);
    }

    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute()
    {
        return $this->stmt->execute();
    }

    public function resultset($json = true)
    {
        $this->execute();
        if ($json) {
            return json_encode($this->stmt->fetchAll(PDO::FETCH_ASSOC));
        } else {
            return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function single($json = true)
    {
        $this->execute();
        if ($json) {
            return json_encode($this->stmt->fetch(PDO::FETCH_ASSOC));
        } else {
            return $this->stmt->fetch(PDO::FETCH_ASSOC);
        }
    }

    public function close()
    {
        $this->pdo = NULL;
    }
}