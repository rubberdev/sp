<?php
session_start();
?>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SP verkiezingdata</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<body>
<div class="container">
    <br>
    <div class="jumbotron">
        <h1>SP verkiezingsdata</h1>
    </div>
    <a href="overview1.php" class="btn btn-lg btn-block btn-primary">Tweede Kamer der Staten-Generaal 2012</a>
    <a href="overview2.php" class="btn btn-lg btn-block btn-success">Gemeenteraad Zwolle 2014</a>
    <a href="overview3.php" class="btn btn-lg btn-block btn-danger">Tweede Kamer der Staten-Generaal 2017</a>
</div>
</body>
</html>